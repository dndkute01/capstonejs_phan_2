function renderTodosList(todos) {
  var contentHTML = "";
  todos.forEach(function (item) {
    var contentTr = `
    <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}$</td>
    <td>
                <img style="width:100px ;" src=${item.img} alt="" />
                </td>
                <td>${item.desc}</td>
                <td>
                    <button class="btn btn-outline-danger" onclick="removeTodo('${item.id}')">Xóa</button>
                    <button class="btn btn-outline-info" onclick="editTodo('${item.id}')">Xem</button>
                </td>
    </tr>
    `;
    contentHTML += contentTr;
  });

  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}

function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}

function getInfoForm() {
  var name = document.getElementById("TenSP").value;
  var price = document.getElementById("GiaSP").value;
  var img = document.getElementById("HinhSP").value;
  var desc = document.getElementById("MoTa").value;
  return {
    name: name,
    price: price,
    img: img,
    desc: desc,
  };
}
